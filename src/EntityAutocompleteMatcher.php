<?php

namespace Drupal\jsonrpc_autocomplete;

use Drupal\Core\Entity\EntityAutocompleteMatcherInterface;
use Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Matcher class to get autocompletion results for entity reference.
 */
class EntityAutocompleteMatcher implements EntityAutocompleteMatcherInterface {

  /**
   * The entity reference selection handler plugin manager.
   *
   * @var \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface
   */
  protected SelectionPluginManagerInterface $selectionManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs an EntityAutocompleteMatcher object.
   *
   * @param \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface $selection_manager
   *   The entity reference selection handler plugin manager.
   */
  public function __construct(SelectionPluginManagerInterface $selection_manager, EntityTypeManagerInterface $entityTypeManager) {
    $this->selectionManager = $selection_manager;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritDoc}
   */
  public function getMatches($target_type, $selection_handler, $selection_settings, $string = '') {
    $matches = [];

    $options = $selection_settings + [
      'target_type' => $target_type,
      'handler' => $selection_handler,
    ];
    $handler = $this->selectionManager->getInstance($options);

    if (isset($string)) {
      // Get an array of matching entities.
      $match_operator = !empty($selection_settings['match_operator']) ? $selection_settings['match_operator'] : 'CONTAINS';
      $match_limit = isset($selection_settings['match_limit']) ? (int) $selection_settings['match_limit'] : 10;
      $entity_labels = $handler->getReferenceableEntities($string, $match_operator, $match_limit);

      $ids = [];
      foreach ($entity_labels as $values) {
        foreach ($values as $entity_id => $label) {
          $ids[] = $entity_id;
        }
      }

      // Load the entities again, this duplicates the load done in
      // getReferenceableEntities() but means we don't have to worry about
      // overriding all instances of getReferenceableEntities() to return the
      // correct format.
      $entities = $this->entityTypeManager->getStorage($target_type)->loadMultiple($ids);

      // Keep the label from the selection handler and enrich the return data
      // with enough info to call json api later if desired.
      foreach ($entities as $entity) {
        $label = $entity_labels[$entity->bundle()][$entity->id()];

        $matches[] = [
          'id' => $entity->id(),
          'uuid' => $entity->uuid(),
          'type' => $entity->getEntityType()->id(),
          'bundle' => $entity->bundle(),
          'label' => $label
        ];
      }
    }

    return $matches;
  }

}
